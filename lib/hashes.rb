def word_lengths(str)
  word_hash = {}
  str.split(" ").each do |word|
    word_hash[word] = word.length
  end
  word_hash
end

def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}[-1][0]
end

def update_inventory(older, newer)
  newer.each do |key, value|
    next if older[key] == newer[key]
    older[key] = value
  end
  older
end

def letter_counts(word)
  letter_hash = Hash.new(0)
  word.split("").each do |char|
    letter_hash[char] ? letter_hash[char] += 1 : letter_hash[char] = 1
  end
  letter_hash
end

def my_uniq(arr)
  elements = Hash.new(false)
  arr.each do |el|
    next if elements[el]
    elements[el] = true
  end
  elements.collect {|k, v| k}
end

def evens_and_odds(numbers)
  counter = {
    even: 0,
    odd: 0
  }
  numbers.each {|x| x%2==0 ? counter[:even] += 1 : counter[:odd] += 1 }
  counter
end

def most_common_vowel(string)
  vowel_counter = Hash.new(0)
  string.split("").each do |char|
    next if !%w{a e i o u}.include?(char)
    vowel_counter[char] ? vowel_counter[char] += 1 : vowel_counter[char] = 1
  end
  greatest_key_by_val(vowel_counter)
end

def fall_and_winter_birthdays(students)
  students = students.select { |k, v| v >= 7}
  names = students.keys
  result = []
  names.each_index do |idx1|
    ((idx1+1)..names.length-1).each do |idx2|
      result << [names[idx1], names[idx2]]
    end
  end
  result
end

def biodiversity_index(specimens)
  uniq_specimens = specimens.uniq

  species_count = {}

  uniq_specimens.each do |spec|
    species_count[spec] = specimens.count(spec)
  end
  fewest_species = species_count.values.min
  most_species = species_count.values.max

  uniq_specimens.length**2 * fewest_species/most_species

end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  can_vandalize = true
  normal_hash = letter_counts(normal_sign)
  vandal_hash = letter_counts(vandalized_sign)
  normal_hash.each do |k, v|
    if normal_hash[k] < vandal_hash[k]
      return false
    end
  end
  can_vandalize
end

def character_count(str)
end
